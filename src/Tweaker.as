const string title = "\\$d36" + Icons::Wrench + "\\$z Tweaker";

Mania@ game;
Window@ window;

void RenderMenu() {
    if (!Setting_EmbeddedInterface && UI::MenuItem(title, "", window.isOpened)) {
        window.isOpened = !window.isOpened;
    }
}

void RenderInterface()
{
    if (window.isOpened) {
		window.Render();
		game.ApplySettings();
	}
}

void Render()
{
	game.Render();
	game.Routine();
	if (game.initialised) {
		if (game.app.GameScene is null) {
			game.RemoveNods();
		}
	} else {
		game.AddNods();
	}
}

void Update(float dt)
{
	game.Update(dt);
}

UI::InputBlocking OnKeyPress(bool down, VirtualKey key)
{
	return game.OnKeyPress(down, key);
}

void Main()
{
	@game = Mania();
	@window = Window();
}
