namespace ZClipDistance
{
    const int Min = 10;
    const int Max = 2500;
}

[Setting hidden]
bool Setting_ZClip = false;

#if SIG_REGULAR
[Setting hidden]
bool Setting_ZClipAsync = false;
#endif

[Setting hidden]
int Setting_ZClipDistance = ZClipDistance::Max;

[Setting hidden]
Shortcut Setting_ZClipShortcut = Shortcut::Disabled;

[Setting hidden]
VirtualKey Setting_ZClipShortcutKey = VirtualKey::F9;