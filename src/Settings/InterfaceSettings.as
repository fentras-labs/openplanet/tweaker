[Setting hidden]
bool Setting_FPS = false;

#if SIG_REGULAR
[Setting hidden]
#endif
OverlayScaling Setting_OverlayScaling = OverlayScaling::Default;

enum OverlayScaling
{
    Default = -1,
    None = 0,
    Resize_Horizontal = 1,
    Resize_Vertical = 2,
    Keep_Ratio = 4,
    Keep_Ratio_Wide = 5
}