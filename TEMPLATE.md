# Plugin Description Template

### openplanet.dev
* Description
* Preview
* Features
* Limitations
* [Source Code](#source-code)
* Credits

#### Source Code
You can access source code on [GitLab](https://gitlab.com/fentrasLABS/openplanet/tweaker).

---

### fentras.com
* Features
* Limitations
* Screenshots
* [Download](#download)
* Credits

#### Download
* [Releases](https://gitlab.com/fentrasLABS/openplanet/tweaker/-/releases)

##### Stable
* [Openplanet](https://openplanet.dev/plugin/tweaker)
* [Source](https://gitlab.com/fentrasLABS/openplanet/tweaker)

##### Preview
* [Openplanet](https://openplanet.dev/plugin/tweakerpreview)
* [Source](https://gitlab.com/fentrasLABS/openplanet/tweaker/-/tree/preview)
